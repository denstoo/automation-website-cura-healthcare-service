package keyword

import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI


public class WebUl {
	public static openBrowser(String url) {
		boolean isOpened = false
		try {
			isOpened = WebUI.verifyElementPresent(new TestObject().addProperty("xpath", ConditionType.EQUALS, "/html/body"), 0)
		} catch (Exception e) { }
		if (isOpened == false) {
			WebUI.openBrowser(url)
		} else {
			WebUI.comment("Starting 'Chrome' driver")
		}
	}
	public static navigateToUrl(String url) {
		//		if (WebUI.getUrl() == 'data:,' ) {
		WebUI.navigateToUrl(url)
//				}
	}
}
