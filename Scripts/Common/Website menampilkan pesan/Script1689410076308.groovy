import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

String actual = WebUI.getText(new TestObject().addProperty("css", ConditionType.EQUALS, ".text-danger"))

WebUI.comment(actual)

WebUI.verifyMatch(actual, pesan, false)

