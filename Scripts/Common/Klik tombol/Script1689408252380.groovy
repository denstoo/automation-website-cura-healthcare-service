import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

WebUI.click(new TestObject().addProperty("xpath", ConditionType.EQUALS, "//*[text()='$locator' and contains(@class,'btn')]"))