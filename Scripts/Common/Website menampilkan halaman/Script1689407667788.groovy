import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

return WebUI.waitForElementPresent(new TestObject().addProperty("xpath", ConditionType.EQUALS, "//h1[text()='$locator'] | //h2[text()='$locator']"), 3, FailureHandling.OPTIONAL)
