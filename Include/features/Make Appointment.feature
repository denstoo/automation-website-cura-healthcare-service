#Author: denstoo@gmail.com
Feature: Make Appointment
  Saya ingin melakukan Make Appointment

  Background: 
    Given Saya membuka website CURA Healthcare Service
    And Website menampilkan halaman "CURA Healthcare Service"
    And Saya klik tombol Make Appointment
    And Saya berada di halaman Make Appointment

  @positive
  Scenario Outline: <scenario>
    When Website menampilkan halaman "Make Appointment"
    And Saya memilih facility = "<facility>"
    And Saya memilih readmission = "<readmission>"
    And Saya memilih programs = "<programs>"
    And Saya memasukan date = "<date>"
    And Saya memasukan comment = "<comment>"
    And Saya klik tombol Book Appointment
    Then Website menampilkan halaman "Appointment Confirmation"
    And Verifikasi facility = "<facility>"
    And Verifikasi readmission = "<readmission>"
    And Verifikasi program = "<programs>"
    And Verifikasi comment = "<comment>"

    Examples: 
      | scenario                                                                  | facility                        | readmission | programs | date       | comment             |
      | E2E Make Appointment facility <facility> dengan readmission <readmission> | Tokyo CURA Healthcare Center    | Yes         | None     | 19/07/2023 | Ini adalah komentar |
      | E2E Make Appointment facility <facility> dengan readmission <readmission> | Tokyo CURA Healthcare Center    | No          | None     | 19/07/2023 | Ini adalah komentar |
      | E2E Make Appointment facility <facility> dengan readmission <readmission> | Hongkong CURA Healthcare Center | Yes         | None     | 19/07/2023 | Ini adalah komentar |
      | E2E Make Appointment facility <facility> dengan readmission <readmission> | Hongkong CURA Healthcare Center | No          | None     | 19/07/2023 | Ini adalah komentar |
      | E2E Make Appointment facility <facility> dengan readmission <readmission> | Seoul CURA Healthcare Center    | Yes         | None     | 19/07/2023 | Ini adalah komentar |
      | E2E Make Appointment facility <facility> dengan readmission <readmission> | Seoul CURA Healthcare Center    | No          | None     | 19/07/2023 | Ini adalah komentar |
      | E2E Make Appointment programs <programs>                                  | Tokyo CURA Healthcare Center    | Yes         | Medicare | 19/07/2023 | Ini adalah komentar |
      | E2E Make Appointment programs <programs>                                  | Tokyo CURA Healthcare Center    | Yes         | Medicaid | 19/07/2023 | Ini adalah komentar |
      | E2E Make Appointment programs <programs>                                  | Tokyo CURA Healthcare Center    | Yes         | None     | 19/07/2023 | Ini adalah komentar |

  @negative
  @datekosong
  Scenario Outline: <scenario>
    When Website menampilkan halaman "Make Appointment"
    And Saya memilih facility = "<facility>"
    And Saya memilih readmission = "<readmission>"
    And Saya memilih programs = "<programs>"
    And Saya memasukan date = "<date>"
    And Saya memasukan comment = "<comment>"
    And Saya klik tombol Book Appointment
    Then Website menyorot "date picker"

    Examples: 
      | scenario                                 | facility                     | readmission | programs | date | comment             |
      | E2E Make Appointment date kosong         | Tokyo CURA Healthcare Center | Yes         | None     | null | Ini adalah komentar |
      | E2E Make Appointment input date masalalu | Tokyo CURA Healthcare Center | Yes         | None     | 02/06/1997 | Ini adalah komentar |
