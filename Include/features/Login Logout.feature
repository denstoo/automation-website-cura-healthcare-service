#Author: denstoo@gmail.com
Feature: Login & Logout
  Saya ingin testing fitur login dan logout

  Background: 
    Given Saya membuka website CURA Healthcare Service
    And Website menampilkan halaman "CURA Healthcare Service"
    And Saya klik tombol Make Appointment
    And Saya berada di halaman Login
    

  @positive
  Scenario: Saya ingin melakukan login yang benar
    When Website menampilkan halaman "Login"
    And Saya memasukan username = "John Doe"
    And Saya memasukan password = "ThisIsNotAPassword"
    And Saya klik tombol Login
    Then Website menampilkan halaman "Make Appointment"
    And Saya klik tombol icon menu
    And Saya klik tombol "Logout"

  @negative
  Scenario Outline: <scenario>
    When Website menampilkan halaman "Login"
    And Saya memasukan username = "<username>"
    And Saya memasukan password = "<password>"
    And Saya klik tombol Login
    Then Website menampilkan pesan "Login failed! Please ensure the username and password are valid."

    Examples: 
      | scenario                               | username       | password           |
      | Login dengan password salah            | John Doe       | password salah     |
      | Login dengan username salah            | username salah | ThisIsNotAPassword |
      | Login tanpa isi password               | John Doe       | null               |
      | Login tanpa isi username               | null           | ThisIsNotAPassword |
      | Login tanpa isi username dan password  | null           | null               |
      | Login dengan username & password salah | username salah | password salah     |
#Scenario: Handling
#When Website menampilkan halaman "Login"
