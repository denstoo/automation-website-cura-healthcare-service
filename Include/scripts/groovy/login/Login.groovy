package login
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import cucumber.api.java.en.And
import scripts.Common

class Login {
	def step = new Common()
	
	@And('Saya berada di halaman Login')
	def Saya_berada_di_halaman_Login() {
		if (step.Website_menampilkan("halaman", "Login") == false) {
			step.Saya_klik_tombol("icon menu")
			step.Saya_klik_tombol('"Logout"')
			step.Saya_klik_tombol('Make Appointment')
		}
	}
	@And('Saya berada di halaman Make Appointment')
	def Saya_berada_di_halaman_Make_Appointment() {
		if (step.Website_menampilkan("halaman", "Make Appointment") == false) {
			step.Saya_klik_tombol('Make Appointment')
			step.Saya("memasukan", "username", "John Doe")
			step.Saya("memasukan", "password", "ThisIsNotAPassword")
			step.Saya_klik_tombol("Login")
		}
	}
}