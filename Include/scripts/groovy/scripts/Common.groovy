package scripts
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class Common {
	@Given('Saya membuka website (.*)')
	def Saya_membuka_website(String web) {
		String url = web =="CURA Healthcare Service" ? "https://katalon-demo-cura.herokuapp.com/" : web
		WebUI.callTestCase(findTestCase('Test Cases/Common/Membuka Website'), ['url' : url])
	}
	@And('Website menampilkan (.*) "(.*)"')
	def Website_menampilkan(String type, String value) {
		boolean result
		if (type == 'halaman') {
			result = WebUI.callTestCase(findTestCase('Test Cases/Common/Website menampilkan halaman'), ['locator' : value])
		} else if (type == 'pesan') {
			WebUI.callTestCase(findTestCase('Test Cases/Common/Website menampilkan pesan'), ['pesan' : value])
		}
		WebUI.takeScreenshot()
		return result
	}
	@And('Saya klik tombol (.*)')
	def Saya_klik_tombol(String locator) {
		WebUI.takeScreenshot()
		if (locator.length() >=4 && locator.substring(0, 4) == 'icon') {
			locator = locator.replaceAll('icon', '').trim()
			WebUI.click(new TestObject().addProperty("xpath", ConditionType.EQUALS, "//*[contains(@id,'$locator') and contains(@class,'btn')]"))
		} else if (locator.substring(0, 1) == '"') {
			locator = locator.replaceAll('"', '')
			WebUI.click(new TestObject().addProperty("xpath", ConditionType.EQUALS, "//a[text()='$locator']"))
		} else {
			WebUI.callTestCase(findTestCase('Test Cases/Common/Klik tombol'), ['locator' : locator])
		}
		WebUI.takeScreenshot()
	}

	@And('Saya (.*) (.*) = "(.*)"')
	def Saya(String type, String locator, String value) {
		value = value == 'null' ? '' : value
		if (type == 'memasukan') {
			WebUI.setText(new TestObject().addProperty("xpath", ConditionType.EQUALS, "//input[contains(@name, '$locator')] | //textarea[contains(@name, '$locator')]"), value)
		} else if (type == 'memilih') {
			String actualType = WebUI.getAttribute(new TestObject().addProperty("xpath", ConditionType.EQUALS, "//*[contains(@name, '$locator')]"), 'type')
			WebUI.comment(actualType)
			if (actualType == 'select-one') {
				WebUI.selectOptionByValue(new TestObject().addProperty("xpath", ConditionType.EQUALS, "//*[@name='$locator']"), value, false)
			} else if (actualType == 'radio') {
				WebUI.click(new TestObject().addProperty("xpath", ConditionType.EQUALS, "//*[@name='$locator' and @value='$value']"))
			} else if (actualType == 'checkbox') {
				String actialRadio = WebUI.getAttribute(new TestObject().addProperty("xpath", ConditionType.EQUALS, "//input[@type='checkbox' and contains(@name,'$locator')]"), 'checked')
				boolean actualChecked = actialRadio == 'true' ? true : false
				boolean checked = value.toLowerCase() == 'yes' ? true : false
				if (checked != actualChecked) {
					WebUI.click(new TestObject().addProperty("xpath", ConditionType.EQUALS, "//input[@type='checkbox' and contains(@name,'$locator')]"))
				}
			}
		}
	}
	@And('Verifikasi (.*) = "(.*)"')
	def Verifikasi(String id, String expected) {
		WebUI.verifyElementText(new TestObject().addProperty("xpath", ConditionType.EQUALS, "//p[contains(@id,'$id')]"), expected)
	}
	@And('Saya menutup browser')
	def Saya_menutup_browser() {
		WebUI.closeBrowser()
	}

	@Then('Website menyorot "(.*)"')
	def Website_menyorot(String locator) {
		locator = "." + locator.replaceAll(" ", "")
		WebUI.verifyElementVisible(new TestObject().addProperty("css", ConditionType.EQUALS, "$locator"))
	}


}